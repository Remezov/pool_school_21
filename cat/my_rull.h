/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   my_rull.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gshira <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 15:56:04 by gshira            #+#    #+#             */
/*   Updated: 2020/02/20 15:58:21 by gshira           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MY_RULL_H
# define MY_RULL_H

# include <fcntl.h>
# include <unistd.h>
# include <errno.h>

void	ft_putchar(char c);
void	ft_putstr(char *str);

#endif
