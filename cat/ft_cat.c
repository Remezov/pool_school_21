/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cat.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gshira <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 15:59:01 by gshira            #+#    #+#             */
/*   Updated: 2020/02/20 18:10:34 by gshira           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "my_rull.h"

void	print(int fd)
{
	char	buffer[1];

	while (read(fd, buffer, 1))
		ft_putchar(*buffer);
}

int		main(int argc, char **argv)
{
	int	fd;
	int	i;

	i = 1;
	while (i < argc)
	{
		fd = open(argv[i++], O_RDWR);
		if (fd < 0)
		{
			if (errno == 2)
				ft_putstr("No such file or directory.");
			if (errno == 13)
				ft_putstr("Permission denied.");
			if (errno == 21)
				ft_putstr("Is a directory.");
			return (0);
		}
		print(fd);
	}
	close(fd);
	return (0);
}
