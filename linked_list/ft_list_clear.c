/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_clear.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gshira <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/19 16:08:49 by gshira            #+#    #+#             */
/*   Updated: 2020/02/19 18:40:20 by gshira           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdlib.h>

void	ft_list_clear(t_list **begin_list)
{
	t_list	*buf;

	while ((*begin_list)->next)
	{
		buf = (*begin_list);
		(*begin_list) = (*begin_list)->next;
		free(buf);
	}
	*begin_list = 0;
}
