/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_params.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gshira <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/19 13:56:55 by gshira            #+#    #+#             */
/*   Updated: 2020/02/19 16:06:13 by gshira           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void		ft_list_push_front(t_list **begin_list, void *data)
{
	t_list *begin;

	begin = ft_create_elem(data);
	begin->next = *begin_list;
	*begin_list = begin;
}

t_list		*ft_list_push_params(int ac, char **av)
{
	t_list	*new_list;
	int		i;

	new_list = 0;
	if (ac == 0)
		return (0);
	i = 0;
	while (i < ac)
	{
		ft_list_push_front(&new_list, av[i]);
		i++;
	}
	return (new_list);
}
