/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_at.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gshira <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/19 19:01:21 by gshira            #+#    #+#             */
/*   Updated: 2020/02/19 19:31:35 by gshira           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

t_list	*ft_list_at(t_list *begin_list, unsigned int nbr)
{
	unsigned int	count;
	t_list			*buf;

	count = 1;
	if (!begin_list || !nbr)
		return (0);
	buf = begin_list;
	while (buf->next && count < nbr)
	{
		buf = buf->next;
		count++;
	}
	if (count != nbr)
		return (0);
	return (buf);
}
