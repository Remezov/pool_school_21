/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   my_rull.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gshira <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 12:46:28 by gshira            #+#    #+#             */
/*   Updated: 2020/02/20 16:41:37 by gshira           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MY_RULL_H
# define MY_RULL_H

# include <fcntl.h>
# include <unistd.h>

void	ft_putchar(char c);
void	ft_putstr(char *str);

#endif
