/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_file.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gshira <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 13:10:07 by gshira            #+#    #+#             */
/*   Updated: 2020/02/20 14:19:13 by gshira           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "my_rull.h"

int	main(int argc, char **argv)
{
	int				fd;
	char			buffer[1];

	if (argc > 2)
	{
		ft_putstr("Too many arguments.");
		return (0);
	}
	fd = open(argv[1], O_RDONLY);
	if (fd < 0)
	{
		ft_putstr("File name missing.");
		return (0);
	}
	while (read(fd, buffer, 1))
		ft_putchar(*buffer);
	close(fd);
	return (0);
}
