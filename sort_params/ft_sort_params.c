/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gshira <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/12 09:45:48 by gshira            #+#    #+#             */
/*   Updated: 2020/02/12 12:53:29 by gshira           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

int		ft_strcmp(char *s1, char *s2)
{
	int i;

	i = 0;
	while (s1[i] == s2[i] && s1[i] != '\0' && s2[i] != '\0')
	{
		i++;
	}
	return (s1[i] - s2[i]);
}

void	ft_swap(int *a, int *b)
{
	int temp;

	temp = *a;
	*a = *b;
	*b = temp;
}

void	print(int argc, char *argv[])
{
	int i_arg;
	int i_argv;

	i_arg = 1;
	while (i_arg < argc)
	{
		i_argv = 0;
		while (argv[i_arg][i_argv] != '\0')
		{
			ft_putchar(argv[i_arg][i_argv]);
			i_argv++;
		}
		ft_putchar('\n');
		i_arg++;
	}
}

int		main(int argc, char *argv[])
{
	int i;

	i = 1;
	while (i < argc - 1)
	{
		if (ft_strcmp(argv[i], argv[i + 1]) > 0)
		{
			ft_swap(&argv[i], &argv[i + 1]);
			i = 1;
		}
		else
		{
			i++;
		}
	}
	print(argc, argv);
	return (0);
}
