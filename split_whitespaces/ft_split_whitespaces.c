/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_whitespaces.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gshira <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/15 12:49:34 by gshira            #+#    #+#             */
/*   Updated: 2020/02/15 20:43:04 by gshira           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		count_words(char *str)
{
	int	i_s;
	int	count_w;

	i_s = -1;
	count_w = 0;
	while (str[++i_s])
	{
		if (str[i_s] != ' ' && str[i_s] != '\t' && str[i_s] != '\n' &&
				i_s == 0)
			count_w++;
		else if ((str[i_s] == ' ' || str[i_s] == '\t' || str[i_s] != '\n') &&
				i_s == 0)
			continue;
		else if ((str[i_s] != ' ' && str[i_s] != '\t' && str[i_s] != '\n') &&
				(str[i_s - 1] == ' ' || str[i_s - 1] == '\t' ||
				str[i_s - 1] == '\n'))
			count_w++;
	}
	return (count_w);
}

char	**create_subpointers(char **words, char *str, int i_w)
{
	int	i_s;
	int	i_l;

	i_s = -1;
	i_l = 0;
	while (str[++i_s])
	{
		if (str[i_s] != ' ' && str[i_s] != '\t' && str[i_s] != '\n')
			i_l++;
		else if (str[i_s] != ' ' && str[i_s] != '\t' && str[i_s] != '\n' &&
				i_s == 0)
			continue;
		else if ((str[i_s] == ' ' || str[i_s] == '\t' || str[i_s] != '\n') &&
				(str[i_s - 1] != ' ' && str[i_s - 1] != '\t' &&
				str[i_s - 1] != '\n'))
		{
			words[i_w] = malloc(sizeof(char) * i_l + 1);
			i_w++;
			i_l = 0;
		}
	}
	if (i_l != 0)
		words[i_w] = malloc(sizeof(char) * i_l + 1);
	return (words);
}

char	**fill_array(char **words, char *str, int i_w, int i_l)
{
	int		i_s;

	i_s = -1;
	while (str[++i_s])
	{
		if (str[i_s] != ' ' && str[i_s] != '\t' && str[i_s] != '\n')
		{
			words[i_w][i_l] = str[i_s];
			i_l++;
		}
		else if ((str[i_s] == ' ' || str[i_s] == '\t' || str[i_s] == '\n') &&
				i_s == 0)
			continue;
		else if ((str[i_s] == ' ' || str[i_s] == '\t' || str[i_s] != '\n') &&
				(str[i_s - 1] != ' ' && str[i_s - 1] != '\t' &&
				str[i_s - 1] != '\n'))
		{
			words[i_w++][i_l] = '\0';
			i_l = 0;
		}
	}
	if (i_l != 0)
		i_w++;
	words[i_w] = 0;
	return (words);
}

char	**ft_split_whitespaces(char *str)
{
	char	**words;

	words = malloc(sizeof(char*) * (count_words(str) + 1));
	words = create_subpointers(words, str, 0);
	words = fill_array(words, str, 0, 0);
	return (words);
}
